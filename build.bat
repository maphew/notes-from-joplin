@if exist .\public rmdir /s/q .\public

@echo off
for %%g in (mdbook.exe) do (if "%%~dp$PATH:g"=="" (
      echo. %%g not found. Run 'scoop install mdbook'
      goto :eof
      ))
@echo on

mdbook build -d public

python ./move_html_to_dir public/

@REM -- Not implemented --
@REM # Make checkboxes editable, just in case the user wants to keep their own checklist.
@REM find public/ -name "*.html" -type f -exec sed -i 's/input disabled=""/input/g' {} +

xcopy /s static\* public\
