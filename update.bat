@echo off
REM Ensure python in path
for %%g in (python.exe) do (
  if "%%~dp$PATH:g"=="" call conda activate )

setlocal
set _p=%prompt%
set prompt=::$s
@echo on

git pull
python .\joplinexport
git add .
git diff --cached

@echo off
REM This yields "git: 'cma' is not a git command". A personal macro?
REM git cma Updates
set prompt=%_p%
